import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { YoutubePlayerMiniModule }  from 'ng2-youtube-player-mini'
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { Vibration } from '@ionic-native/vibration';
import { AndroidFullScreen } from '@ionic-native/android-full-screen';

import { TwitterService } from 'ng2-twitter';

import { MyApp } from './app.component';
import { HomePage, ModalContentPage } from '../pages/home/home';
import { SetupPage } from '../pages/setup/setup';

import { ColorPickerModule } from 'ngx-color-picker';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SetupPage,
    ModalContentPage
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    YoutubePlayerMiniModule,
    ColorPickerModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SetupPage,
    ModalContentPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Vibration,
    AndroidFullScreen,
    TwitterService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})

export class AppModule {}
