var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, NgModule } from '@angular/core';
import { ModalController, NavController, Platform, NavParams, ViewController } from 'ionic-angular';
import { YoutubeapiService } from './youtubeapi.service';
import { FormControl } from '@angular/forms';
import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Vibration } from '@ionic-native/vibration';
import { AndroidFullScreen } from '@ionic-native/android-full-screen';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, youtube, http, modalCtrl, vibration, http2, androidFullScreen) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.youtube = youtube;
        this.http = http;
        this.modalCtrl = modalCtrl;
        this.vibration = vibration;
        this.http2 = http2;
        this.androidFullScreen = androidFullScreen;
        this.search = new FormControl();
        this.theValue = '';
        this.deletecount = 0;
        this.today = Date.now();
        setInterval(function () {
            _this.today = Date.now();
            console.log(_this.today);
        }, 60000);
        this.loadspin = false;
        this.dashmode = false;
        this.aimode = false;
        this.alertflash = false;
        this.canviewdash = false;
        this.searchmode = false;
        this.coremode = false;
        this.settingsmode = false;
        this.gserachmode = false;
        this.splashscreen = true;
        this.selectedCritter = 'tiger';
        this.critters = ['tiger', 'sloth', 'penguin', 'bunny', 'fox', 'glider', 'hedgehog', 'koala', 'lion', 'mouse', 'owl', 'panda', 'possum', 'seal', 'snail', 'squirrl'];
        this.nighttime = false;
        this.results = null;
        this.selectedSearch = 'everything';
        this.videoPlayer = this.search.valueChanges.debounceTime(100).switchMap(function (query) { return youtube.search(query); });
        this.androidFullScreen.isImmersiveModeSupported().then(function () { return _this.androidFullScreen.immersiveMode(); }).catch(function (error) { return console.log(error); });
        this.AltmetricSearch();
        this.EverywhereSearch();
    }
    HomePage.prototype.userTypeing = function ($event) {
        console.log($event);
        if ($event == 'typescript' || $event == 'Typescript') {
            this.modal = this.modalCtrl.create(ModalContentPage);
            this.modal.present();
        }
    };
    HomePage.prototype.gofullm = function () {
        this.fullinfo = true;
        this.theValue = this.theValue.substring(0, this.theValue.length - 1);
        var theValue = this.theValue;
        this.artsy(theValue);
    };
    HomePage.prototype.ungofullm = function () {
        if (this.fullinfo) {
            this.fullinfo = false;
        }
    };
    HomePage.prototype.selectCritter = function (critter) {
        this.selectedCritter = critter;
    };
    HomePage.prototype.unlockDevice = function () {
        var _this = this;
        console.log("Device Unlocking...");
        this.unlockedDev = true;
        setTimeout(function () {
            _this.coremode = true;
            _this.splashscreen = false;
            console.log("Device Unlocked");
        }, 300);
    };
    HomePage.prototype.settingssmode = function () {
        this.settingsmode = true;
        this.gserachmode = false;
    };
    HomePage.prototype.gsearchmode = function () {
        this.gserachmode = true;
        this.settingsmode = false;
    };
    HomePage.prototype.unsettingssmode = function () {
        this.settingsmode = false;
    };
    HomePage.prototype.ungsearchmode = function () {
        this.gserachmode = false;
    };
    HomePage.prototype.lockDevice = function () {
        this.unlockedDev = false;
        this.coremode = false;
        this.splashscreen = true;
    };
    HomePage.prototype.useKeyboard = function (key) {
        this.vibration.vibrate(50);
        if (this.hitshift) {
            this.theValue = this.theValue + key.toUpperCase();
        }
        else {
            this.theValue = this.theValue + key;
        }
        var str = this.theValue;
        var theValue = str.replace(/ /g, "%20");
        this.WikiSearch(theValue);
        console.log(theValue);
    };
    HomePage.prototype.useBackspace = function () {
        this.vibration.vibrate(80);
        this.theValue = this.theValue.substring(0, this.theValue.length - 1);
        var theValue = this.theValue;
        this.WikiSearch(theValue);
    };
    HomePage.prototype.activateshift = function () {
        this.vibration.vibrate(50);
        if (this.hitshift) {
            this.hitshift = false;
        }
        else {
            this.hitshift = true;
        }
    };
    HomePage.prototype.moreicons = function () {
        this.vibration.vibrate(50);
        if (this.specialkeys) {
            this.specialkeys = false;
        }
        else {
            this.specialkeys = true;
        }
    };
    HomePage.prototype.WikiSearch = function (theValue) {
        var _this = this;
        console.log('https://en.wikipedia.org/w/api.php?format=json&action=query&prop=images|info|extracts&exintro=&explaintext=&titles=' + theValue + '&redirects=1');
        this.http2.get('https://en.wikipedia.org/w/api.php?format=json&action=query&prop=images|info|extracts&exintro=&explaintext=&titles=' + theValue + '&redirects=1').subscribe(function (data) {
            _this.extractwiki = data;
            _this.otherResults = data;
            _this.otherResults = Array.of(_this.otherResults);
        }, function (err) { return console.log("Error: " + JSON.stringify(err)); }, function () {
            Object.keys(_this.otherResults).forEach(function (key) {
                if (_this.otherResults[key] && theValue) {
                    _this.otherResults = _this.otherResults[key].query.pages;
                    for (var key in _this.otherResults) {
                        if (_this.otherResults.hasOwnProperty(key)) {
                            // console.log(this.otherResults[key])
                            _this.titlewiki = _this.otherResults[key].title;
                            _this.extractwiki = _this.otherResults[key].extract;
                            if (_this.otherResults[key].images[0]) {
                                _this.imagewiki = _this.otherResults[key].images[0].title;
                                if (_this.imagewiki) {
                                    _this.http2.get('https://en.wikipedia.org/w/api.php?format=json&action=query&titles=' + _this.imagewiki + '&prop=imageinfo&&iiprop=url&iiurlwidth=220').subscribe(function (data) {
                                        _this.otherResults = data;
                                        _this.otherResults = Array.of(_this.otherResults);
                                    }, function (err) { return console.log(err); }, function () {
                                        Object.keys(_this.otherResults).forEach(function (key) {
                                            var otherResults = _this.otherResults[key].query.pages;
                                            for (var key in otherResults) {
                                                if (otherResults.hasOwnProperty(key)) {
                                                    if (otherResults[key]) {
                                                        _this.imgwik = otherResults[key].imageinfo[0].url;
                                                    }
                                                    break;
                                                }
                                            }
                                        });
                                    });
                                }
                                break;
                            }
                        }
                    }
                }
            });
        });
    };
    HomePage.prototype.artsy = function (theValue) {
        var _this = this;
        this.http2.get('https://api.artsy.net/api/artists/' + theValue).subscribe(function (data) {
            console.log(data);
            _this.otherResults = data;
            _this.otherResults = Array.of(_this.otherResults);
        }, function (err) { return console.log("Error: " + JSON.stringify(err)); }, function () {
            Object.keys(_this.otherResults).forEach(function (key) {
                _this.artits = _this.otherResults[key];
            });
        });
    };
    HomePage.prototype.AltmetricSearch = function () {
        var _this = this;
        this.http2.get('https://api.altmetric.com/v1/citations/1y?order_by=pubdate').subscribe(function (data) {
            var otherResults = data;
            _this.otherResults = Array.of(otherResults);
        }, function (err) { return console.log("Error: " + JSON.stringify(err)); }, function () {
            Object.keys(_this.otherResults).forEach(function (key) {
                _this.AltmetricResults = _this.otherResults[key].results;
                console.log(_this.AltmetricResults);
            });
        });
    };
    HomePage.prototype.EverywhereSearch = function () {
        this.http2.get('https://www.googleapis.com/customsearch/v1?key=AIzaSyBlMGuhX3vpQp7jNVvVkiUl8F5VFhjT2Cg&cx=017576662512468239146:omuauf_lfve&q=pornhub').subscribe(function (data) {
            console.log(data);
            // var otherResults = data;
            // this.otherResults = Array.of(otherResults);
        }, function (err) { return console.log("Error: " + JSON.stringify(err)); }, function () {
            // Object.keys(this.otherResults).forEach(key=> {
            //  this.AltmetricResults = this.otherResults[key].results
            //  console.log(this.AltmetricResults)
            // });
        });
    };
    HomePage.prototype.switchSearchMode = function ($event) {
        var _this = this;
        if ($event == 'everything') {
            console.log("ALL SEARCH");
        }
        else if ($event == 'videos') {
            console.log("VIDEO SEARCH");
            this.results = this.videoPlayer;
        }
        else if ($event == 'books') {
            console.log("BOOK SEARCH");
            this.http.get('http://gutendex.com/books/?search=' + this.search.value).subscribe(function (data) {
                _this.coreBookData = JSON.stringify(_this.coreBookData);
                _this.results = _this.coreBookData;
            });
        }
        else if ($event == 'articles') {
            console.log("ARTICLES SEARCH");
            this.results = this.videoPlayer;
        }
        else {
            console.log("UNKNOWN SEARCH MODE");
            this.results = null;
        }
    };
    HomePage.prototype.goaimode = function (displayName) {
        var _this = this;
        this.loadspin = true;
        this.displayName = displayName.viewModel;
        setTimeout(function () { _this.aimode = true; }, 4000);
        // if(displayName.viewModel){
        // 	this.loadspin = true;
        // 	this.displayName = displayName.viewModel
        //
        // 	setTimeout(()=>{ this.aimode = true; }, 4000)
        // }else{
        // 	this.alertflash = true;
        // }
    };
    ;
    HomePage.prototype.godashmode = function () {
        var _this = this;
        this.loadspin = true;
        setTimeout(function () { _this.dashmode = true; }, 4000);
        setTimeout(function () { _this.canviewdash = true; }, 4900);
    };
    HomePage.prototype.gomodesel = function (country, city, school) {
        var _this = this;
        this.loadspin = true;
        this.dashmode = false;
        this.aimode = false;
        setTimeout(function () { _this.searchmode = true; _this.loadspin = false; }, 4000);
    };
    ;
    HomePage.prototype.nightTime = function () {
        if (this.nighttime) {
            this.nighttime = false;
        }
        else {
            this.nighttime = true;
        }
    };
    ;
    HomePage = __decorate([
        NgModule({
            imports: [FormControl]
        }),
        Component({
            selector: 'page-home',
            templateUrl: 'home.html',
            providers: [YoutubeapiService]
        }),
        __metadata("design:paramtypes", [NavController, YoutubeapiService, Http, ModalController, Vibration, HttpClient, AndroidFullScreen])
    ], HomePage);
    return HomePage;
}());
export { HomePage };
var ModalContentPage = /** @class */ (function () {
    function ModalContentPage(platform, params, viewCtrl) {
        this.platform = platform;
        this.params = params;
        this.viewCtrl = viewCtrl;
    }
    ModalContentPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    ModalContentPage = __decorate([
        Component({
            selector: 'ModalContentPage',
            templateUrl: 'infosheet.html',
        }),
        __metadata("design:paramtypes", [Platform, NavParams, ViewController])
    ], ModalContentPage);
    return ModalContentPage;
}());
export { ModalContentPage };
//# sourceMappingURL=home.js.map