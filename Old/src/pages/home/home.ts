import { Component, NgModule, HostListener } from '@angular/core';
import { ModalController, NavController, Platform, NavParams, ViewController } from 'ionic-angular';
import { SetupPage } from '../setup/setup';

import { Vibration } from '@ionic-native/vibration';
import { AndroidFullScreen } from '@ionic-native/android-full-screen';

import { NgForm, FormControl, ReactiveFormsModule } from '@angular/forms';
import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import {DomSanitizer} from "@angular/platform-browser";

import { YoutubeapiService } from './youtubeapi.service';
import { TwitterService } from 'ng2-twitter';

import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';

@NgModule({
  imports: [ FormControl ]
})

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [YoutubeapiService]
})


export class HomePage {
    search = new FormControl();
    results: Observable < any > ;

    loadspin: boolean;
    dashmode: boolean;
    aimode: boolean;
    canviewdash: boolean;
    nighttime: boolean;
    alertflash: boolean;
    searchmode: boolean;
    coremode: boolean;
    hitshift: boolean;
    specialkeys: boolean;
    fullinfo: boolean;
    splashscreen: boolean;
    unlockedDev: boolean;
    settingsmode: boolean;
    gserachmode: boolean;
    isinwebview: boolean;
    gscholarswitch: boolean;
    isProfileMode: boolean;
    loadingfeature: boolean;
    parentMode: boolean;
    theValue: string = '';
    deletecount: number = 0;

    otherResults: any;
    fresults: any;
    eresults: any;
    critters: any[];
    selectedCritter: any;
    gapi: any;
    videoPlayer: any;
    today: any;

    selectedSearch: string;
    displayName: string;
    bookFinder: any;
    coreBookData: any;
    modal: any;
    timeoutHandler: any;
    titlewiki: string;
    extractwiki: any;
    imagewiki: any;
    artits: any;
    AltmetricResults: any;
    TwitterResults: any;
    gSearchResults: any;
    imgwik: string;
    videoUrl: any;


    titlewikicatch: string;
    extractwikicatch: string;
    extractwikicatchp2: string;
    imgwikcatch: string;
    theValuef: any;
    wikiarticles: any;
    wikiimages: any;
    linkValue: any;
    flicker: any;
    happinessmode: any;



    constructor(public navCtrl: NavController, public youtube: YoutubeapiService, private http: Http, public modalCtrl: ModalController, private vibration: Vibration, private http2: HttpClient, private androidFullScreen: AndroidFullScreen, private twitter: TwitterService, private domSanitizer : DomSanitizer) {
        this.today = Date.now();

        setInterval(() => {
            this.today = Date.now();
        }, 60000)

        this.loadspin = false;
        this.dashmode = false;
        this.aimode = false;
        this.alertflash = false;
        this.canviewdash = false;
        this.searchmode = false;
        this.coremode = false;
        this.settingsmode = false;
        this.gserachmode = false;
        this.splashscreen = true;
        this.parentMode = false;

        this.selectedCritter = 'tiger';

        this.critters = ['tiger', 'sloth', 'penguin', 'bunny', 'fox', 'glider', 'hedgehog', 'koala', 'lion', 'mouse', 'owl', 'panda', 'possum', 'seal', 'snail', 'squirrl']

        this.nighttime = false;

        this.results = null
        this.selectedSearch = 'everything'

        this.videoPlayer = this.search.valueChanges.debounceTime(100).switchMap(query => youtube.search(query));

        this.androidFullScreen.isImmersiveModeSupported().then(() => this.androidFullScreen.immersiveMode()).catch((error: any) => console.log(error));

        var theValue = null;
        this.AltmetricSearch()
        this.WikiSearch(theValue)
        this.twitterSearch(theValue)
        this.flickerSearch(theValue)
    }

    userTypeing($event) {
        console.log($event)

        if ($event == 'typescript' || $event == 'Typescript') {
            this.modal = this.modalCtrl.create(ModalContentPage);
            this.modal.present();
        }
    }

    gofullm(theValue, titlewiki, extractwiki, imgwik) {
        this.fullinfo = true;

        var str = titlewiki;
        var theValue2 = str.replace(/ /g, "%20");
        this.twitterSearch(theValue2)

        this.titlewikicatch = titlewiki;

        var arr = [];
        str = extractwiki.split('\n');
        arr.push(str.shift());
        arr.push(str.join(' '));

        this.extractwikicatch = arr[0]
        this.extractwikicatchp2 = arr[1].replace(/\.(?=[^\d])[ ]*/g, '. \n\n')

        this.imgwikcatch = imgwik;

    }

    ungofullm() {
        if (this.fullinfo) {
            this.fullinfo = false;
        }

        this.settingsmode = false;
        this.gserachmode = false;
        this.gscholarswitch = false;
        this.isProfileMode = false;
    }

    selectCritter(critter) {
        this.selectedCritter = critter
    }

    unlockDevice() {
        console.log("Device Unlocking...")
        this.unlockedDev = true;

        setTimeout(() => {
            this.coremode = true;
            this.splashscreen = false;
            console.log("Device Unlocked")
        }, 300)
    }

    settingssmode() {
        this.settingsmode = true;
        this.gserachmode = false;
    }

    setup(){
      this.navCtrl.push(SetupPage);
    }

    happiness() {
        this.happinessmode = true;
    }

    gsearchmode() {
        this.gserachmode = true;
        this.settingsmode = false;
    }

    unsettingssmode() {
        this.settingsmode = false;
    }

    ungsearchmode() {
        this.gserachmode = false;
    }

    lockDevice() {
        this.unlockedDev = false;
        this.coremode = false;
        this.splashscreen = true;
    }

    useKeyboard(key) {
        this.vibration.vibrate(50);

        if (this.hitshift) {
            this.theValue = this.theValue + key.toUpperCase();
        } else {
            this.theValue = this.theValue + key
        }

        var str = this.theValue;
        var theValue = str.replace(/ /g, "%20");

        if (this.gserachmode) {
            this.EverywhereSearch(theValue)
        }else if(this.gscholarswitch){
          this.ScholarSearch(theValue)
        }else {
            this.WikiSearch(theValue)
            this.flickerSearch(theValue)
        }

    }

    useBackspace() {
        this.vibration.vibrate(80);

        this.theValue = this.theValue.substring(0, this.theValue.length - 1);
        var theValue = this.theValue;

        var str = this.theValue;
        var theValue = str.replace(/ /g, "%20");

        if (this.gserachmode) {
            this.EverywhereSearch(theValue)
        } else {
            this.WikiSearch(theValue)
            this.flickerSearch(theValue)
        }

    }

    activateshift() {
        this.vibration.vibrate(50);

        if (this.hitshift) {
            this.hitshift = false
        } else {
            this.hitshift = true
        }
    }

    moreicons() {
        this.vibration.vibrate(50);

        if (this.specialkeys) {
            this.specialkeys = false
        } else {
            this.specialkeys = true
        }
    }

    gscholarmode() {
        this.gscholarswitch = true;

        this.theValue = this.theValue.substring(0, this.theValue.length - 1);
        var theValue = this.theValue;

        var str = this.theValue;
        var theValue = str.replace(/ /g, "%20");

        this.ScholarSearch(theValue)
    }

    ProfileMode() {
        this.isProfileMode = true;
    }

    WikiSearch(theValue) {
        console.log(this.theValue)

        if (this.theValue.length >= 1) {
            if (!theValue) {
                var sciencearray = ['osmosis', 'acarology', 'fossils', 'minerals', 'temperature', 'volcanology', 'physical science', 'volumetric flask', 'thermometer', 'anaesthesiology', 'anthropobiology', 'electrochemist', 'climate',
                    'astronomy', 'zoology', 'laboratory', 'particle', 'lepidoptery', 'seismology', 'geology', 'genetics', 'theories', 'astacology', 'biochemistry', 'evolution', 'Elon Musk', 'atom', 'electrochemist', 'experiment'
                ]

                var scienceval = sciencearray[Math.floor(Math.random() * sciencearray.length)];
                theValue = scienceval
                this.linkValue = theValue;

                this.bookFinder = setInterval(() => {
                    var scienceval = sciencearray[Math.floor(Math.random() * sciencearray.length)];
                    theValue = scienceval
                    this.linkValue = theValue;

                    this.WikiSearch(theValue)
                    this.flickerSearch(theValue)
                }, 15000)
            } else {
                theValue = theValue

                if (this.bookFinder) {
                    clearInterval(this.bookFinder);
                }
            }
        }else{
            var sciencearray = ['osmosis', 'acarology', 'fossils', 'minerals', 'temperature', 'volcanology', 'physical science', 'volumetric flask', 'thermometer', 'anaesthesiology', 'anthropobiology', 'electrochemist', 'climate',
                'astronomy', 'zoology', 'laboratory', 'particle', 'lepidoptery', 'seismology', 'geology', 'genetics', 'theories', 'astacology', 'biochemistry', 'evolution', 'Elon Musk', 'atom', 'electrochemist', 'experiment'
            ]

            var scienceval = sciencearray[Math.floor(Math.random() * sciencearray.length)];
            theValue = scienceval
            this.linkValue = theValue;

            this.flickerSearch(theValue)
        }


        interface UserResponse {
            query: any;
            pages: any;
        }

        this.http2.get < UserResponse >
            ('https://en.wikipedia.org/w/api.php?format=json&action=query&prop=images|info|extracts&exintro=&explaintext=&titles=' + theValue + '&redirects=1').subscribe(
                data => {
                    this.extractwiki = data;
                    this.otherResults = data;
                    this.otherResults = Array.of(this.otherResults);
                },
                err => console.log("Error: " + JSON.stringify(err)), () => {

                    Object.keys(this.otherResults).forEach(key => {
                        if (this.otherResults[key] && theValue) {
                            this.otherResults = this.otherResults[key].query.pages;

                            for (var key in this.otherResults) {
                                if (this.otherResults.hasOwnProperty(key)) {
                                    // console.log(this.otherResults[key])
                                    this.titlewiki = this.otherResults[key].title;
                                    this.extractwiki = this.otherResults[key].extract;

                                    if (this.otherResults[key].images.length >= 1) {
                                        var theimages = this.otherResults[key].images;
                                    } else {
                                        var theimages = null;
                                    }

                                    var objectcounter = [];

                                    for (var i = 0; i < theimages.length; ++i) {
                                        if (theimages[i].title) {
                                            this.http2.get < UserResponse >
                                                ('https://en.wikipedia.org/w/api.php?format=json&action=query&titles=' + theimages[i].title + '&prop=imageinfo&&iiprop=url&iiurlwidth=480').subscribe(
                                                    data => {
                                                        this.otherResults = data;
                                                        this.otherResults = Array.of(this.otherResults);
                                                    },
                                                    err => console.log("Error: " + JSON.stringify(err)), () => {

                                                        Object.keys(this.otherResults).forEach(key => {
                                                            var longrange = this.otherResults[key].query.pages;
                                                            for (var key in longrange) {
                                                                if (longrange[key].imageinfo) {
                                                                    this.otherResults = longrange[key].imageinfo[0].url;

                                                                    if (this.otherResults == 'https://upload.wikimedia.org/wikipedia/en/4/4a/Commons-logo.svg' || this.otherResults == 'https://upload.wikimedia.org/wikipedia/commons/3/37/People_icon.svg' || this.otherResults == 'https://upload.wikimedia.org/wikipedia/en/f/fd/Portal-puzzle.svg' || this.otherResults == 'https://upload.wikimedia.org/wikipedia/commons/6/65/Lock-green.svg' || this.otherResults == 'https://upload.wikimedia.org/wikipedia/en/4/48/Folder_Hexagonal_Icon.svg' || this.otherResults == 'https://upload.wikimedia.org/wikipedia/commons/8/89/Symbol_book_class2.svg') {} else {

                                                                        objectcounter.push(this.otherResults)
                                                                    }

                                                                }
                                                            }
                                                        });
                                                    }
                                                );
                                        }
                                    }

                                    this.wikiimages = objectcounter
                                    console.log(this.wikiimages)
                                }

                            }
                        }
                    });
                }
            );
    }

    artsy(theValue) {
        interface UserResponse {
            query: any;
            pages: any;
        }

        this.http2.get < UserResponse > ('https://api.artsy.net/api/artists/' + theValue).subscribe(
            data => {
                console.log(data)
                this.otherResults = data;
                this.otherResults = Array.of(this.otherResults);
            },
            err => console.log("Error: " + JSON.stringify(err)), () => {
                Object.keys(this.otherResults).forEach(key => {
                    this.artits = this.otherResults[key];
                });
            }
        );
    }


    flickerSearch(theValue) {

        if (!theValue) {
            var sciencearray = ['osmosis', 'acarology', 'fossils', 'minerals', 'temperature', 'volcanology', 'physical science', 'volumetric flask', 'thermometer', 'anaesthesiology', 'anthropobiology', 'electrochemist', 'climate',
                'astronomy', 'zoology', 'laboratory', 'particle', 'lepidoptery', 'seismology', 'geology', 'genetics', 'theories', 'astacology', 'biochemistry', 'evolution', 'Elon Musk', 'atom', 'electrochemist', 'experiment'
            ]

            var scienceval = sciencearray[Math.floor(Math.random() * sciencearray.length)];
            theValue = scienceval
            this.linkValue = theValue;

            this.bookFinder = setInterval(() => {
                var scienceval = sciencearray[Math.floor(Math.random() * sciencearray.length)];
                theValue = scienceval
                this.linkValue = theValue;
            }, 15000)
        } else {
            theValue = theValue

            if (this.bookFinder) {
                clearInterval(this.bookFinder);
            }
        }

        interface UserResponse {
            query: any;
            pages: any;
        }

        this.http2.get < UserResponse > ('https://www.googleapis.com/customsearch/v1?key=AIzaSyC5fd5sFx7g6XHHLaLl8N7E7uhOm2qD9-g&cx=013483737079049266941:mzydshy4xwi&searchType=image&safe=high&imgSize=huge&q=' + theValue).subscribe(
            data => {
                var otherResults = data;
                this.otherResults = Array.of(otherResults);
            },
            err => console.log("Error: " + JSON.stringify(err)), () => {
                Object.keys(this.otherResults).forEach(key => {
                    console.log(this.otherResults[key])
                    this.flicker = this.otherResults[key].items
                });
            }
        );
    }


    AltmetricSearch() {
        // interface UserResponse {
        //   query: any;
        //   pages: any;
        // }
        //
        // this.http2.get<UserResponse>('https://api.altmetric.com/v1/citations/1y?order_by=readers').subscribe(
        // 	data => {
        // 		var otherResults = data;
        // 		this.otherResults = Array.of(otherResults);
        // 	},
        //     	err => console.log("Error: " + JSON.stringify(err)),
        //     	() => {
        // 	    Object.keys(this.otherResults).forEach(key=> {
        // 		    this.AltmetricResults = this.otherResults[key].results
        // 		    console.log(this.AltmetricResults)
        // 	    });
        //     	}
        //   );
    }

    twitterSearch(theValue2) {
        if (theValue2) {
            this.twitter.get(
                    'https://api.twitter.com/1.1/search/tweets.json', {
                        q: theValue2,
                        result_type: 'popular',
                        lang: 'en'
                    }, {
                        consumerKey: 'XjMMYviKvGgRK2Yo935GzRn3l',
                        consumerSecret: 'IP2DhiVLLUtUNZnABP87kmeCA1apEmsVYNo5jAWnTUOFaGJhDN'
                    }, {
                        token: '4239897252-4PC8t5J0mIhXZe3ksj00CX7NhHqNq7zFvLo3gy8',
                        tokenSecret: 'Rw8qo8IYv0nw1jwn5Wp6GYDUmPug2rXQFgjacxXJfM6IH'
                    }
                ).map(res => res.json())
                .subscribe((res) => {
                    this.TwitterResults = res.statuses;
                    console.log(this.TwitterResults)
                });
        } else {
            this.twitter.get(
                    'https://api.twitter.com/1.1/search/tweets.json', {
                        q: 'science',
                        result_type: 'popular',
                        lang: 'en'
                    }, {
                        consumerKey: 'XjMMYviKvGgRK2Yo935GzRn3l',
                        consumerSecret: 'IP2DhiVLLUtUNZnABP87kmeCA1apEmsVYNo5jAWnTUOFaGJhDN'
                    }, {
                        token: '4239897252-4PC8t5J0mIhXZe3ksj00CX7NhHqNq7zFvLo3gy8',
                        tokenSecret: 'Rw8qo8IYv0nw1jwn5Wp6GYDUmPug2rXQFgjacxXJfM6IH'
                    }
                ).map(res => res.json())
                .subscribe((res) => {
                    this.TwitterResults = res.statuses;
                    console.log(this.TwitterResults)
                });
            //
            //   console.log("Run twitter line")
            //     this.twitter.get(
            //       'https://stream.twitter.com/1.1/statuses/filter.json',
            //       {
            //         track: 'twitter'
            //       },
            //       {
            //         consumerKey: 'XjMMYviKvGgRK2Yo935GzRn3l',
            //         consumerSecret: 'IP2DhiVLLUtUNZnABP87kmeCA1apEmsVYNo5jAWnTUOFaGJhDN'
            //       },
            //       {
            //         token: '4239897252-4PC8t5J0mIhXZe3ksj00CX7NhHqNq7zFvLo3gy8',
            //         tokenSecret: 'Rw8qo8IYv0nw1jwn5Wp6GYDUmPug2rXQFgjacxXJfM6IH'
            //       }
            //   )
            //   .subscribe((res)=>{
            // 		    this.TwitterResults = res;
            // 		    console.log(res)
            //   });
        }
    }

    ScholarSearch(theValue) {
        this.loadingfeature = true;

        var usevalue;

        if(!this.linkValue){
          usevalue = this.theValue
        }else{
          usevalue = this.linkValue
        }

        console.log(usevalue);

        interface UserResponse {
            query: any;
            pages: any;
        }

        this.http2.get < UserResponse > ('https://www.googleapis.com/customsearch/v1?key=AIzaSyC5fd5sFx7g6XHHLaLl8N7E7uhOm2qD9-g&cx=017576662512468239146:omuauf_lfve&q=' + usevalue).subscribe(
            data => {
                var otherResults = data;
                this.otherResults = Array.of(otherResults);
            },
            err => console.log("Error: " + JSON.stringify(err)), () => {
                Object.keys(this.otherResults).forEach(key => {
                    this.gSearchResults = this.otherResults[key].items
                    console.log(this.gSearchResults)
                    this.loadingfeature = false;
                });
            }
        );
    }

    EverywhereSearch(theValue) {
        interface UserResponse {
            query: any;
            pages: any;
        }

        this.http2.get < UserResponse > ('https://www.googleapis.com/customsearch/v1?key=AIzaSyC5fd5sFx7g6XHHLaLl8N7E7uhOm2qD9-g&cx=017576662512468239146:omuauf_lfve&q=' + theValue).subscribe(
            data => {
                var otherResults = data;
                this.otherResults = Array.of(otherResults);
            },
            err => console.log("Error: " + JSON.stringify(err)), () => {
                Object.keys(this.otherResults).forEach(key => {
                    this.gSearchResults = this.otherResults[key].items
                    console.log(this.gSearchResults)
                });
            }
        );
    }

    webviewr(gSearchResult) {
        this.isinwebview = true;

        var searchResult = gSearchResult.formattedUrl
        this.videoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(searchResult);
        console.log("Viewing Webpage: " + this.videoUrl)
    }

    goTeachMode() {
      this.parentMode = true;
    }

    ungoTeachMode() {
      this.parentMode = false;
    }

    switchSearchMode($event) {
        if ($event == 'everything') {
            console.log("ALL SEARCH")

        } else if ($event == 'videos') {
            console.log("VIDEO SEARCH")
            this.results = this.videoPlayer
        } else if ($event == 'books') {
            console.log("BOOK SEARCH")

            this.http.get('http://gutendex.com/books/?search=' + this.search.value).subscribe(data => {
                this.coreBookData = JSON.stringify(this.coreBookData)
                this.results = this.coreBookData;
            });
        } else if ($event == 'articles') {
            console.log("ARTICLES SEARCH")
            this.results = this.videoPlayer
        } else {
            console.log("UNKNOWN SEARCH MODE")
            this.results = null
        }
    }

    goaimode(displayName) {
        this.loadspin = true;
        this.displayName = displayName.viewModel

        setTimeout(() => {
            this.aimode = true;
        }, 4000)

        // if(displayName.viewModel){
        // 	this.loadspin = true;
        // 	this.displayName = displayName.viewModel
        //
        // 	setTimeout(()=>{ this.aimode = true; }, 4000)
        // }else{
        // 	this.alertflash = true;
        // }

    };

    godashmode() {
        this.loadspin = true;

        setTimeout(() => {
            this.dashmode = true;
        }, 4000)

        setTimeout(() => {
            this.canviewdash = true;
        }, 4900)

    }


    gomodesel(country, city, school) {
        this.loadspin = true;
        this.dashmode = false;
        this.aimode = false;

        setTimeout(() => {
            this.searchmode = true;
            this.loadspin = false;
        }, 4000)
    };

    nightTime() {
        if (this.nighttime) {
            this.nighttime = false;
        } else {
            this.nighttime = true;

        }
    };

    // onSubmit(f: NgForm) {
    //     console.log(f.value.searchTerm);

    // 	interface UserResponse {
    // 	  query: any;
    // 	  pages: any;
    // 	}

    // 	this.http2.get<UserResponse>
    // 	('https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro=&explaintext=&titles='+ f.value.searchTerm +'&redirects=1').subscribe(
    // 		data => {
    //  			this.otherResults = data;
    //  			this.otherResults = Array.of(this.otherResults);
    // 		},
    //       	err => console.log("Error occured: " + err),
    //       	() => {
    //       		Object.keys(this.otherResults).forEach(key=> {
    // 			    this.otherResults = this.otherResults[key].query.pages
    // 			    console.log(this.otherResults)
    // 			});
    //       	}
    //     );
    // };
}

@Component({
    selector: 'ModalContentPage',
    templateUrl: 'infosheet.html',
})

export class ModalContentPage {

    constructor(public platform: Platform, public params: NavParams, public viewCtrl: ViewController) {

    }

    dismiss() {
        this.viewCtrl.dismiss();
    }
}
