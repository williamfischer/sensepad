var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
var BASE_URL = 'https://www.googleapis.com/youtube/v3/search';
var API_TOKEN = 'AIzaSyA5J6tCTUpt8B8NoiBIVmU2403GNw_LlHw';
var YoutubeapiService = /** @class */ (function () {
    function YoutubeapiService(http) {
        this.http = http;
    }
    YoutubeapiService.prototype.search = function (query) {
        return this.http.get(BASE_URL + "?q=" + query + "&part=snippet&safeSearch=strict&key=" + API_TOKEN)
            .map(function (res) { return res.json(); })
            .map(function (json) { return json.items; });
    };
    YoutubeapiService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Http])
    ], YoutubeapiService);
    return YoutubeapiService;
}());
export { YoutubeapiService };
//# sourceMappingURL=youtubeapi.service.js.map