import { Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';

const BASE_URL = 'https://www.googleapis.com/youtube/v3/search';
const API_TOKEN = 'AIzaSyA5J6tCTUpt8B8NoiBIVmU2403GNw_LlHw';

@Injectable()
export class YoutubeapiService{

	constructor(private http:Http) {}

	search(query){
    	return this.http.get(`${BASE_URL}?q=${query}&part=snippet&safeSearch=strict&key=${API_TOKEN}`)
			.map((res:Response) => res.json())
			.map(json => json.items);
	}
}
