import { Component, NgModule, HostListener } from '@angular/core';
import { ModalController, NavController, Platform, NavParams, ViewController } from 'ionic-angular';

import { NgForm, FormControl, ReactiveFormsModule } from '@angular/forms';
import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';

import { HomePage } from '../home/home';

@NgModule({
  imports: [ FormControl ]
})

@Component({
  selector: 'page-setup',
  templateUrl: 'setup.html'
})


export class SetupPage {
    otherResults: any;
    ColorResults: any;
    private color: string = "#F7D6B5";

    constructor(public navCtrl: NavController, private http: Http, public modalCtrl: ModalController, private http2: HttpClient) {


      interface UserResponse {
          query: any;
          pages: any;
      }

      this.http2.get < UserResponse > ('http://www.colourlovers.com/api/colors/new?format=json&numResults=100').subscribe(
          data => {
              this.otherResults = data;
              this.otherResults = Array.of(this.otherResults);
          },
          err => console.log("Error: " + JSON.stringify(err)),
          () => {
              Object.keys(this.otherResults).forEach(key => {
                  this.otherResults = this.otherResults[key]

                  var thecolors = [];

                  thecolors.push('specialone')

                  for (var key in this.otherResults) {
                      if (this.otherResults.hasOwnProperty(key)) {
                        thecolors.push('#' + this.otherResults[key].hex)
                      }
                  }

                  this.ColorResults = thecolors;
                  console.log(this.ColorResults)

              });
          }
      );

    }

    goBack(){
      this.navCtrl.push(HomePage);
    }


}
