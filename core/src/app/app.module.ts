import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingComponent } from './landing/landing.component';

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyD56ICp4zSOMJJa-P0FY1qt6lln2jONzag",
    authDomain: "sense-5a533.firebaseapp.com",
    databaseURL: "https://sense-5a533.firebaseio.com",
    projectId: "sense-5a533",
    storageBucket: "sense-5a533.appspot.com",
    messagingSenderId: "709572505526",
    appId: "1:709572505526:web:ec60b2021e11c49ebb1b17"
  }
};

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
