import { Component, OnInit } from '@angular/core';

import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase';


@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  user : any;
  biteCollection: any;

  creteBite: boolean;
  biteStep: number;

  bite: any = {
    title : '',
    description: '',
    subject : '',
    subSubject : '',
    one: {
      slideOne : '',
      slideTwo : '',
      slideThree : '',
      question : '',
      answer : '',
    },
    two: {
      slideOne : '',
      slideTwo : '',
      slideThree : '',
      question : '',
      answer : '',
    },
    three: {
      slideOne : '',
      slideTwo : '',
      slideThree : '',
      question : '',
      answer : '',
    }
  }

  constructor(public afAuth: AngularFireAuth, public fireStore: AngularFirestore) {


    this.afAuth.authState.subscribe(auth => {
      if(auth){
        this.user = auth;
      }
    });

    this.biteCollection = this.fireStore.collection('Bites').valueChanges().subscribe(
    allBites =>{
      console.log(allBites)
    });

  }

  ngOnInit(): void {

  }

  accessLogin(){
    this.afAuth.signInWithPopup(new firebase.auth.GoogleAuthProvider()).then(function(result) {
      this.userLogged = result.user;
      console.log(this.userLogged)
    }, function(error) {
      alert(error);
    });
  }

  logOut(){

  }

  createBite(){
    this.creteBite = true;
  }

  submitBite(){
    console.log(this.bite);

    let bitAddress = this.fireStore.doc('Bites/' + this.user.uid + '/' + this.bite.subject + '/' + this.bite.title);

    bitAddress.set(this.bite,{
      merge: true
    });

    bitAddress.set({
      teacherID: this.user.uid
    },{
      merge: true
    });
  }

  nextBite(bite){
    console.log('To step #' + bite);
    this.biteStep = Number(bite)
  }

  goHome(){
    this.biteStep = 0;

    this.creteBite = false;
  }

  previousSlide(){
    this.biteStep = this.biteStep - 1;
  }

  nextSlide(){
    if(!this.biteStep){
      this.biteStep = 1;
    }else{
      this.biteStep = +this.biteStep + 1;
    }
  }

}
