import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  startContent:boolean;
  nameSelect: boolean;

  constructor() {
    this.startContent = true;
  }

  nameSelectTrigger(){
    this.startContent = false;
    this.nameSelect = true;
  }
}
